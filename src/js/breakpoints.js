
let breakpoints = {
    small: '300px',
    medium: '600px',
    large: '992px',
    xlarge: '1200px',
}

export default breakpoints