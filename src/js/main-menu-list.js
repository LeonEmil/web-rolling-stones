let mainMenuList = [
    {
        text: "Home",
        href: "/",
        style: "main-menu__link",
    },
    {
        text: "Live",
        href: "/live",
        style: "main-menu__link",
    },
    {
        text: "News",
        href: "/news",
        style: "main-menu__link",
    },
    {
        text: "Store",
        href: "https://the-rolling-stones.lnk.to/StoreWE",
        style: "main-menu__link",
    },
    {
        text: "Sign Up",
        href: "https://www.rollingstones.com/sign-up/",
        style: "main-menu__link",
    },
    {
        text: "Social",
        href: "#a",
        style: "main-menu__link",
    },
    {
        text: "Buy honk",
        href: "https://the-rolling-stones.lnk.to/HonkWE",
        style: "button",
    },
]

export default mainMenuList