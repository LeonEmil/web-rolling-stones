
let img = {
    logo: {
        title: "logo",
        src: "https://d2ydxeblbafldb.cloudfront.net/img/global/logo/rs.png",
        alt: "Rollings stones logo",
        style: "logo"
    },
    headerImage: {
        title: "Header",
        src: 'https://res.cloudinary.com/leonemil/image/upload/v1580496552/Rolling%20stones/honk-small.png',
        srcMedium: "https://res.cloudinary.com/leonemil/image/upload/v1580496562/Rolling%20stones/header.png",
        alt: "Imagen de encabezado. Logo de hook",
        style: "header-image",
    },
}


export default img