import React from 'react'
import img from '../../js/images'
import breakpoints from '../../js/breakpoints'

const Header = () => {
    return(
        <section>
            <picture>
                <source media={`(min-width:${breakpoints.medium})`} srcset={img.headerImage.srcMedium} />
                <img src={img.headerImage.src} alt={img.headerImage.alt} />
            </picture>
        </section>
    )
}

export default Header