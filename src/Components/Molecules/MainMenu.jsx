import React from "react"
import { NavLink } from 'react-router-dom'
import img from "../../js/images";
import mainMenuList from "../../js/main-menu-list"


const MainMenu = () => {
    return(
        <nav className="main-menu">
            <div className="main-menu__logo-container">
                <NavLink to="/">
                    <img src={img.logo.src} alt={img.logo.alt} className={img.logo.style}/>
                </NavLink>
            </div>
            <ul className="main-menu__list">
                {
                    mainMenuList.map(item => {
                        return <li><NavLink to={item.href} className={item.style}>{item.text}</NavLink></li>
                    })
                }
            </ul>
            <div className="main-menu__toggle"></div>
        </nav>
    )
}

export default MainMenu