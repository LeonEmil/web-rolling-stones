import React from "react"
import mainMenuList from "../../js/main-menu-list"
//import MainMenuItem from "../Atoms/MainMenuItem"

const MainMenuList = () => {
    return(
        <ul>
            {
                mainMenuList.map(item => {
                    return <li><a href={item.href}>{item.text}</a></li>
                })
            }
        </ul>
    )
}

export default MainMenuList