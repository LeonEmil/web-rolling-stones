import React, { Component } from "react"

const WithLoader = (propValue, WrappedComponent) => {
    return class WithLoader extends Component {

        constructor(props) {
            super(props)
        }

        render() {
            return this.props[propValue].lenght === 0
                ? <h1>Cargando...</h1>
                : <WrappedComponent {...this.props} />
        }
    }
}

export default WithLoader