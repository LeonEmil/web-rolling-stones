import React from 'react'

const Button = (href, text, style) => {
    return(
        <a className={style} href={href}>{text}</a>
    )
}

export default Button