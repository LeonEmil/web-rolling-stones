import React from "react"

const MainMenuItem = (text, href) => {
    return(
        <li className="main-menu__item">
            <a className="main-menu__link" href={href}>{text}</a>
        </li>
    )
}

export default MainMenuItem