import React from 'react'
import MainMenu from '../Molecules/MainMenu'
import Header from '../Molecules/Header'

const Home = () => {
    return(
        <>
            <MainMenu />
            <Header />
        </>
    )
}

export default Home