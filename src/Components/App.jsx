import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import '../sass/style.scss';
import Home from './Pages/Home';
import Live from './Pages/Live';
import News from './Pages/News';
import NotFound from './Pages/NotFound'

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/live" exact component={Live} />
        <Route path="/news" exact component={News} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  );
}

export default App;
